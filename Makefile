all: staticlib
	clang++ main.cpp -g -Wall -O0 -std=c++11 tscore.a -lpthread -o bin/exe
net: staticlib
	clang++ net_server.cpp -Wall -std=c++11 -o bin/nets tscore.a
	clang++ net_client.cpp -Wall -std=c++11 -o bin/netc tscore.a
staticlib:
	clang++ -c src/console.cpp -g -Wall -O0 -o obj/console.o
	clang++ -c src/file.cpp -g -Wall -O0 -o obj/file.o
	clang++ -c src/sockets.cpp -g -Wall -O0 -o obj/sockets.o
	clang++ -c src/string.cpp -g -Wall -O0 -o obj/string.o
	clang++ -c src/threads.cpp -g -Wall -O0 -o obj/threads.o
	clang++ -c src/timing.cpp -g -Wall -O0 -o obj/timing.o
	clang++ -c src/vfs.cpp -g -Wall -O0 -o obj/vfs.o
	ar rvs tscore.a obj/*.o

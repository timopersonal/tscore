#include "include/console.h"
#define DBG_LEVEL DBG_LEVEL_ALL
#include "include/debug.h"
#include "include/file.h"
#include "include/platform.h"
#include "include/sockets.h"
#include "include/timing.h"
#include "include/types.h"

using namespace tscore;

int main(int argc, char** argv)
{
	Socket server = Socket(Protocol::UDP, 8080);
	server.Bind();
	server.Listen();

	Connection inc;
	server.Accept(&inc);

	tstring message;
	Connection sender;
	while (message != "/exit\n")
	{
		server.Receive(&message, &sender);
		Print(message);

		server.Send(&sender, message);
	}

	return 0;
}

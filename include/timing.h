#ifndef TSCORE_TIMING_H
#define TSCORE_TIMING_H

namespace tscore
{
	namespace Time
	{
		double GetTime();
	};
}

#endif // TSCORE_TIMING_H

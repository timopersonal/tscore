#ifndef TSCORE_THREADS_H
#define TSCORE_THREADS_H

#include <functional>
#include <thread>
#include <vector>

#include "console.h"
#include "debug.h"
#include "types.h"

namespace tscore
{
	class Thread
	{
	public:
		static void Sleep(uint32_t ms);

		template<typename T, typename ...varg>
		Thread(T (*function)(varg...), varg ... args)
		{
			m_thread = std::thread(function, args...);
			Print(DBG_DEBUG("Successfully created thread."));
		}

		void Await();
	protected:
	private:
		std::thread m_thread;
	};/*

	void testLoop(void)
	{

	}

	template<typename T>
	class ThreadPool
	{
	public:
		ThreadPool(uint8_t numThreads) :
			m_tasks(0)
		{
			m_threads = (Thread*)calloc(numThreads, sizeof(Thread));
			for (int i = 0; i < numThreads; ++i)
			{
				Thread temp = Thread(std::thread(&ThreadPool::MainThreadLoop, this));
			}
		}

		void Enqueue(T t)
		{
			m_tasks.push_back(t);
		}

		~ThreadPool()
		{
			free(m_threads);
		}
	protected:
		void MainThreadLoop(void)
		{
			int testCount = 0;
			printf("yo im here\n");
			while (testCount++ < 10)
			{
				T lastElement = m_tasks[m_tasks.size() - 1];
				m_tasks.erase(m_tasks.begin() + (m_tasks.size() - 1));
				lastElement();
			}
		}
	private:
		Thread* m_threads;
		std::vector<T> m_tasks;
	};
	*/
}
#endif // TSCORE_THREADS_H

#ifndef TSCORE_DEBUG_H
#define TSCORE_DEBUG_H

#define DBG_LEVEL_OFF   0
#define DBG_LEVEL_FATAL 1
#define DBG_LEVEL_ERROR 2
#define DBG_LEVEL_WARN  3
#define DBG_LEVEL_INFO  4
#define DBG_LEVEL_DEBUG 5
#define DBG_LEVEL_ALL   6

#define DBG_LEVEL DBG_LEVEL_ALL

#if DBG_LEVEL >= DBG_LEVEL_FATAL
	#ifdef TSCORE_CONSOLE_H
    	#define DBG_FATAL(x) tscore::String(ANSI_BOLD) + ANSI_RED + tscore::String("FATAL  : ") + x + ANSI_RST
	#else
    	#define DBG_FATAL(x) tscore::String("FATAL  : ") + x
	#endif
#else
    #define DBG_FATAL(x) ""
#endif

#if DBG_LEVEL >= DBG_LEVEL_ERROR
	#ifdef TSCORE_CONSOLE_H
    	#define DBG_ERROR(x) ANSI_RED + tscore::String("ERROR  : ") + x + ANSI_RST
	#else
    	#define DBG_ERROR(x) tscore::String("ERROR  : ") + x
	#endif
#else
    #define DBG_ERROR(x) ""
#endif

#if DBG_LEVEL >= DBG_LEVEL_WARN
	#ifdef TSCORE_CONSOLE_H
    	#define DBG_WARN(x) ANSI_YEL + tscore::String("WARN   : ") + x + ANSI_RST
	#else
    	#define DBG_WARN(x) tscore::String("WARN   : ") + x
	#endif
#else
    #define DBG_WARN(x) ""
#endif

#if DBG_LEVEL >= DBG_LEVEL_INFO
	#ifdef TSCORE_CONSOLE_H
    	#define DBG_INFO(x) tscore::String("INFO   : ") + x
	#else
    	#define DBG_INFO(x) tscore::String("INFO   : ") + x
	#endif
#else
    #define DBG_INFO(x) ""
#endif

#if DBG_LEVEL >= DBG_LEVEL_DEBUG
	#ifdef TSCORE_CONSOLE_H
    	#define DBG_DEBUG(x) ANSI_CYN + tscore::String("DEBUG  : ") + x + ANSI_RST
	#else
    	#define DBG_DEBUG(x) tscore::String("DEBUG  : ") + x
	#endif
#else
    #define DBG_DEBUG(x) ""
#endif

#if DBG_LEVEL == DBG_LEVEL_ALL
	#ifdef TSCORE_CONSOLE_H
    	#define DBG_TRACE(x) ANSI_BLU + tscore::String("TRACE  : ") + x + ANSI_RST
	#else
    	#define DBG_TRACE(x) tscore::String("TRACE  : ") + x
	#endif
#else
    #define DBG_TRACE(x) ""
#endif

#endif // TSCORE_DEBUG_H

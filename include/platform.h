#ifndef TSCORE_PLATFORM_H
#define TSCORE_PLATFORM_H

#if defined(unix) || defined(__unix__) || defined(__unix)
	#define PLAT_UNIX
#elif defined(_WIN32) || defined(_WIN64)
	#define PLAT_WIN
#endif

#endif // TSCORE_PLATFORM_H

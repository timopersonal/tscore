#ifndef TIMO_SOCKET_H
#define TIMO_SOCKET_H

#include "../include/types.h"
#ifdef _WIN32
#include <WinSock2.h>
#else
#include <netinet/in.h>
#include <sys/socket.h>
#define SOCKET_ERROR -1
#endif

namespace tscore
{
	enum Protocol {
		TCP,
		UDP
	};

	struct Connection
	{
		friend class Socket;
	public:
		uint32_t address;
		uint16_t port;
	protected:
		sockaddr_in socket;
	};

	class Socket
	{
	public:
		Socket(Protocol protocol, uint32_t port);
		~Socket();

		int8_t Bind();
		int8_t Listen();
		int8_t Accept(Connection* pConnection);
		int8_t Connect(const String& address);

		Connection* GetConnection() { return &m_serverConnection; }

		int8_t Send(Connection* dest, const String& message);
		int8_t Receive(tstring* pMessage, Connection* pConnection);
	protected:
	private:
	#ifdef _WIN32
		static WSADATA s_wsaData;
	#endif
		static int32_t Init();

		int32_t m_socket;
		Protocol m_protocol;
		uint32_t m_port;
		sockaddr_in m_address;
		uint16_t m_bufferSize;
		Connection m_serverConnection;

		int32_t m_connectedSocket;
	};
}

#endif // TIMO_SOCKET_H

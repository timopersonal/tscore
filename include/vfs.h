#ifndef TSCORE_VFS_H
#define TSCORE_VFS_H

#include "file.h"

#include <unordered_map>

namespace tscore
{
	class VFS
	{
	public:
		VFS(const String& root = "");

		int Mount(const String& physicalPath, const String& virtualPath = "");
		ReadFile* OpenFile(const String& fileName);
		String GetRealPath(const String& virtualPath) { return m_vfsMap.at(virtualPath); }
	protected:
	private:
		std::unordered_map<String, String> m_vfsMap;
	};
}

#endif // TSCORE_VFS_H

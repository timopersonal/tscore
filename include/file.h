#ifndef TSCORE_FILE_H
#define TSCORE_FILE_H

#define READ_BUFFER_LENGTH 512

#include "types.h"

namespace tscore
{
	class IFile
	{
	public:
		IFile();
		virtual ~IFile();

		int MoveCursor(long int offset, int mode = SEEK_CUR);
		unsigned int GetCursor() { return ftell(m_pFile); }
		unsigned int GetFileLength() { return m_fileLength; }
	protected:
		FILE* m_pFile;
		String m_fileName;
		unsigned int m_fileLength;
	private:
	};

	class WriteFile : public IFile
	{
	public:
		enum WriteMode {
			WRITE_CREATEONLY,
			WRITE_OVERWRITE,
			WRITE_APPEND
		};

		WriteFile(const String& fileName, bool binary = false, WriteMode writeMode = WRITE_CREATEONLY);
		~WriteFile();

		bool Write(const String& stream);
	protected:
	private:
	};

	class ReadFile : public IFile
	{
	public:
		ReadFile(const String& fileName, bool binary = false);
		~ReadFile();

		String Read(unsigned int n);
		bool IterateLine(bool cursorEndLine = true);
		String currentLine;
	protected:
	private:
		char currentLineBuffer[READ_BUFFER_LENGTH];
	};
}

#endif // TSCORE_FILE_H

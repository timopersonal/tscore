#ifndef TSCORE_CONFIGFILE_H
#define TSCORE_CONFIGFILE_H

#include "file.h"
#include "types.h"

#include <unordered_map>

class ConfigFile
{
public:
	ConfigFile(const tscore::String& configName) :
		m_configName(configName)
	{
		tscore::ReadFile openFile = tscore::ReadFile(configName);

		while (openFile.IterateLine())
		{
			tscore::String curLine = openFile.currentLine;
			size_t kvDiv = curLine.find('=');
			if (kvDiv != tstring::npos)
			{
				tscore::String key = curLine.substr(0, kvDiv);
				tscore::String value = curLine.substr(kvDiv + 1, curLine.length() - (kvDiv + 1));
				m_valueMap[key] = value;
			}
		}

		tscore::Print(DBG_DEBUG("Successfully copied configurations to memory."));
	}

	void Write(const tscore::String& configName)
	{
		tscore::WriteFile openFile = tscore::WriteFile(configName, false, tscore::WriteFile::WRITE_OVERWRITE);
		for (auto it : m_valueMap)
		{
			openFile.Write(it.first + "=" + it.second + "\n");
		}

		tscore::Print(DBG_DEBUG("Successfully written config file to \'" + configName + "\'."));
	}

	tscore::String GetProperty(const tscore::String& key)
	{
		if (m_valueMap.find(key) != m_valueMap.end())
			return m_valueMap.at(key);
		else
			return "NULL";
	}

	bool AddProperty(const tscore::String& key, const tscore::String& value)
	{
		if (m_valueMap.find(key) != m_valueMap.end())
		{
			tscore::Print(DBG_WARN("Property \'" + key + "\' already exists in \'" + m_configName + "\'."));
			return false;
		}
		else
			m_valueMap.insert(std::pair<tscore::String, tscore::String>(key, value));

		tscore::Print(DBG_WARN("Successfully added property \'" + key + "\' to \'" + m_configName + "\'."));
		return true;
	}

	~ConfigFile()
	{
		Write(m_configName);
	}
protected:
private:
	tscore::String m_configName;
	std::unordered_map<tscore::String, tscore::String> m_valueMap;
};

#endif // TSCORE_CONFIGFILE_H

#ifndef TSCORE_TYPES_H
#define TSCORE_TYPES_H

/* Basic string typedef'd to String */
#include <string>
typedef std::basic_string<char> tstring;
#define ToString(x) String(std::to_string(x).c_str())

#include "string.h"

#include <stdint.h>

#endif // TSCORE_TYPES_H

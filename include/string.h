#ifndef TSCORE_STRING_H
#define TSCORE_STRING_H

#include "../include/types.h"

namespace tscore
{
	class String
	{
	public:
		String();
		String(const char* inChars);
		~String();

		size_t length() const { return m_length; }
		const char* c_str() const { return m_charArray; }
		const char* data() { return m_charArray; }
		char at(size_t pos) const { return m_charArray[pos]; }

		size_t find(char c, size_t pos = 0) const;
		String substr(size_t offset, size_t n) const;

		void resize(size_t size);

		String operator=(const String& rhs);
		char operator[](size_t pos) const { return at(pos); }

		void operator+=(const String& other);
		String operator+(const String& other) const;
		friend String operator+(const char* lhs, const String& rhs);

		bool operator==(const String& rhs) const;

		static const size_t npos = 18446744073709551615UL;
	protected:
	private:
		size_t m_length;
		size_t m_capacity;
		char* m_charArray;

		void increase_capacity(size_t cap);
	};
};

namespace std
{
    template<> struct hash<tscore::String>
    {
        std::size_t operator()(const tscore::String& s) const noexcept
        {
			return std::hash<tstring>()(s.c_str());
        }
    };
}

#endif // TSCORE_STRING_H
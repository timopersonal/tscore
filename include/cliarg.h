#ifndef TSCORE_CLIARG_H
#define TSCORE_CLIARG_H

#include <algorithm>
#include <vector>

#include "console.h"
#include "debug.h"
#include "types.h"

namespace tscore
{
	class CLIArguments
	{
	public:
		CLIArguments(int argc, char** argv)
		{
			for (int i = 0; i < argc; ++i)
			{
				m_argMap.push_back(argv[i]);
			}

			Print(DBG_DEBUG("Successfully converted program parameters."));
		}

		CLIArguments(const tscore::String& input)
		{
			tscore::String remStr = input + " ";
			size_t spaceIndex = 0;

			while ((spaceIndex = remStr.find(' ')) != tscore::String::npos)
			{
				size_t quotIndex = remStr.find('\"');
				size_t bsIndex = remStr.find('\\');
				if (quotIndex < spaceIndex)
				{
					size_t quotEnd = remStr.find('\"', quotIndex + 1);
					m_argMap.push_back(remStr.substr(quotIndex + 1, quotEnd - (quotIndex + 1)));
					remStr = remStr.substr(quotEnd + 2, remStr.length() - (quotEnd + 2));
				}
				else if (bsIndex + 1 == spaceIndex)
				{
					size_t sepSpace = 0;
					while ((sepSpace = remStr.find(' ', sepSpace + 1)) != tscore::String::npos)
					{
						if (remStr.at(sepSpace - 1) != '\\')
						{
							tstring finalString = remStr.substr(0, sepSpace).c_str();
							finalString.erase(std::remove(finalString.begin(), finalString.end(), '\\'), finalString.end());
							m_argMap.push_back(finalString.c_str());
							remStr = remStr.substr(sepSpace + 1, remStr.length() - (sepSpace + 1));
						}
					}
				}
				else
				{
					m_argMap.push_back(remStr.substr(0, spaceIndex));
					remStr = remStr.substr(spaceIndex + 1, remStr.length() - (spaceIndex + 1));
				}
			}

			Print(DBG_DEBUG("Successfully converted program input."));
		}

		tscore::String GetValue(const tscore::String& key)
		{
			std::vector<tscore::String>::iterator it = std::find(m_argMap.begin(), m_argMap.end(), "-" + key);
			if (it != m_argMap.end())
			{
				if (it < m_argMap.end() - 1)
				{
					if ((it + 1)->at(0) != '-')
						return *(++it);
				}
				else
					return *it;
			}

			return "NULL";
		}

		size_t GetNumArgs() { return m_argMap.size(); }
		tscore::String operator[](unsigned int index) { return m_argMap[index]; }
	protected:
	private:
		std::vector<tscore::String> m_argMap;
	};
}

#endif // TSCORE_CLIARG_H

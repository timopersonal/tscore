#ifndef TSCORE_CONSOLE_H
#define TSCORE_CONSOLE_H

#include "string.h"
#include "types.h"

namespace tscore
{
	/// I/O
//	void Print(const tstring& x);
	void Print(const String& x);

	/// Colors
	#define ANSI_RST "\x1B[0m"
	#define ANSI_RED "\x1B[31m"
	#define ANSI_GRN "\x1B[32m"
	#define ANSI_YEL "\x1B[33m"
	#define ANSI_BLU "\x1B[34m"
	#define ANSI_MAG "\x1B[35m"
	#define ANSI_CYN "\x1B[36m"
	#define ANSI_WHT "\x1B[37m"

	#define ANSI_BOLD "\x1B[1m"
	#define ANSI_UNDL "\x1B[4m"

	#define TERM_RED(x) ANSI_RED x ANSI_RST
	#define TERM_GRN(x) ANSI_GRN x ANSI_RST
	#define TERM_YEL(x) ANSI_YEL x ANSI_RST
	#define TERM_BLU(x) ANSI_BLU x ANSI_RST
	#define TERM_MAG(x) ANSI_MAG x ANSI_RST
	#define TERM_CYN(x) ANSI_CYN x ANSI_RST
	#define TERM_WHT(x) ANSI_WHT x ANSI_RST

	#define TERM_BOLD(x) ANSI_BOLD x ANSI_RST
	#define TERM_UNDL(x) ANSI_UNDL x ANSI_RST
}

#endif // TSCORE_CONSOLE_H

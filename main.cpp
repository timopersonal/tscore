#include "include/cliarg.h"
#include "include/configfile.h"
#include "include/console.h"
#include "include/debug.h"
#include "include/file.h"
#include "include/platform.h"
#include "include/sockets.h"
#include "include/string.h"
#include "include/threads.h"
#include "include/timing.h"
#include "include/types.h"
#include "include/vfs.h"

using namespace tscore;

int testThread(double a, double b)
{
	Print("Multithreaded Calculations:");
	for (int i = 0; i < 3; ++i)
	{
		Print("\t" + ToString(a * (b + i)));
	}
	return 0;
}

void SectionTitle(const String& title)
{
	String printStr = "\n" + title + "\n";
	for (int i = 0; i < title.length(); ++i)
	{
		printStr += "=";
	}

	Print(printStr);
}

int main(int argc, char** argv)
{
	double bootTime = Time::GetTime();

	SectionTitle("Platform");
	#ifdef PLAT_UNIX
		Print("Linux");
	#else
		Print("Windows");
	#endif

	SectionTitle("CLI Arguments");
	CLIArguments argParser = CLIArguments("./bin/exe -w 800 -h 600 -t \"Title Test With Spaces\" -d This\\ is\\ a\\ test\\ description -fs");
	for (int i = 0; i < argParser.GetNumArgs(); ++i)
	{
		Print(argParser[i]);
	}

	SectionTitle("Debugging");
	{
		Print(DBG_FATAL("Test"));
		Print(DBG_ERROR("Test"));
		Print(DBG_WARN("Test"));
		Print(DBG_INFO("Test"));
		Print(DBG_DEBUG("Test"));
		Print(DBG_TRACE("Test"));
	}

	SectionTitle("Virtual File System");
	VFS vfs = VFS("res");
	vfs.Mount("res/vfstest", "test");
	{
		ReadFile* rf = vfs.OpenFile("test/test.test");
		while (rf->IterateLine())
		{
			Print(rf->currentLine);
		}
	}

	SectionTitle("File Output");
	{
		WriteFile wf = WriteFile("res/test.test", false, WriteFile::WRITE_OVERWRITE);
		wf.Write("This file is created through the WriteFile class.\nAnd now we're reading it!");
		wf.MoveCursor(13, SEEK_SET);
		wf.Write("written");
	}

	SectionTitle("File Input");
	{
		ReadFile* rf = vfs.OpenFile("test.test");
		while (rf->IterateLine())
		{
			Print(rf->currentLine);
		}
	}

	SectionTitle("Threading");
	Thread thr = Thread(testThread, 0.5, 4.0);
	thr.Await();

	SectionTitle("Timing");
	Print("Sleeping for 1 second...");
	Thread::Sleep(1000);
	Print(DBG_DEBUG("Time since start: " + ToString(Time::GetTime() - bootTime) + "sec"));

	SectionTitle("Custom Strings");
	String testString = "asdf";
	Print(testString);
	return 0;
}

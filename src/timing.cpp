#include "../include/timing.h"
#include "../include/platform.h"

#include <time.h>

#ifdef PLAT_UNIX
	#include <sys/time.h>
	static const long NANOSECONDS_PER_SECOND = 1000000000L;
#else
	#include <chrono>
	static std::chrono::system_clock::time_point m_epoch = std::chrono::high_resolution_clock::now();
#endif

namespace tscore
{
	double Time::GetTime()
	{
		#ifdef PLAT_UNIX
			timespec ts;
			clock_gettime(CLOCK_REALTIME, &ts);
			return (double)(((long) ts.tv_sec * NANOSECONDS_PER_SECOND) + ts.tv_nsec)/((double)(NANOSECONDS_PER_SECOND));
		#else
			return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - m_epoch).count() / 1000000000.0;
		#endif
	}
}

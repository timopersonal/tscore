#include "../include/sockets.h"
/*
#include <thread>
#include <atomic>
#include <mutex>
*/
#ifdef _WIN32
#include <WS2tcpip.h>
#include <Windows.h>
#pragma comment(lib,"ws2_32")

#define errno WSAGetLastError()

WSADATA Socket::s_wsaData;
#else
#include <arpa/inet.h>
#include <errno.h>
#include <netinet/ip.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define closesocket close
#endif

#include "../include/console.h"
#include "../include/debug.h"

size_t socketSize = sizeof(sockaddr_in);

namespace tscore
{
	int32_t Socket::Init()
	{
	#ifdef _WIN32
		int32_t res = WSAStartup(MAKEWORD(2, 2), &s_wsaData);
		if (res != NO_ERROR)
		{
			Print(DBG_ERROR("Failed to startup WSA! (" + ToString(res) + ")"));
			return res;
		}

		Print(DBG_INFO("Initialized WSA!"));
		return 0;
	#else
		Print(DBG_INFO("Socket initialized!"));
		return 0;
	#endif
	}

	Socket::Socket(Protocol protocol, uint32_t port) :
		m_protocol(protocol),
		m_port(port)
	{
	#ifdef _WIN32
		if (s_wsaData.wVersion == NULL)
			Init();
	#endif

		m_socket = (int32_t)socket(AF_INET, protocol + 1, 0);
		if (m_socket == 0)
		{
			int error = errno;
			Print(DBG_ERROR("Failed to open socket! (" + String(ToString(error).c_str()) + ")"));
			exit(error);
		}

		if (protocol == Protocol::UDP)
			m_bufferSize = 1024;
		else
			m_bufferSize = 1024;

		Print(DBG_INFO("Created socket!"));
	}

	Socket::~Socket()
	{
		closesocket(m_socket);
	#ifdef _WIN32
		WSACleanup();
	#endif
	}

	int8_t Socket::Bind()
	{
		m_address.sin_family = AF_INET;
		m_address.sin_addr.s_addr = INADDR_ANY; // TODO: Add variable value
		m_address.sin_port = htons(m_port);

		int8_t res = bind(m_socket, (sockaddr*)&m_address, sizeof(m_address));
		if (res == SOCKET_ERROR)
		{
			res = errno;
			Print(DBG_ERROR("Failed to bind server to port " + ToString(m_port) + " (" + ToString(res) + ")"));
			return res;
		}

		Print(DBG_INFO("Bound server to port " + ToString(m_port)));
		return 0;
	}

	int8_t Socket::Listen()
	{
		if (m_protocol == Protocol::TCP)
		{
			int8_t res = listen(m_socket, 0);
			if (res == SOCKET_ERROR)
			{
				res = errno;
				Print(DBG_ERROR("Listening (" + ToString(res) + ")"));
				return res;
			}
		}

		return 0;
	}

	int8_t Socket::Accept(Connection* pConnection)
	{
		if (m_protocol == Protocol::TCP)
		{
			m_connectedSocket = accept(m_socket, (struct sockaddr*)&pConnection->socket, (unsigned int*)&socketSize);
			if (m_connectedSocket == SOCKET_ERROR)
			{
				int res = errno;
				Print(DBG_ERROR("Failed to accept incoming connection! (" + ToString(res) + ")"));
				return res;
			}
		}

		return 0;
	}

	int8_t Socket::Connect(const String& address)
	{
		m_serverConnection.socket = { 0 };
		m_serverConnection.socket.sin_family = AF_INET;
		m_serverConnection.socket.sin_port = htons(m_port);

		int8_t res = inet_pton(AF_INET, address.c_str(), &m_serverConnection.socket.sin_addr);
		if (res <= 0)
		{
			res = errno;
			Print(DBG_WARN("Invalid IPv4 address!" + ToString(res)));
			return res;
		}

		res = connect(m_socket, (sockaddr*)&m_serverConnection.socket, socketSize);
		if (res == SOCKET_ERROR)
		{
			res = errno;
			Print(DBG_ERROR("Failed to reach server at " + address + " (" + ToString(res) + ")"));
			return res;
		}

		Print(DBG_INFO("Connected to " + address));
		return 0;
	}

	int8_t Socket::Send(Connection* dest, const String& message)
	{
		int8_t res = -1;
		res = sendto(m_socket, message.c_str(), (int)message.length(), 0, (struct sockaddr*)&dest->socket, sizeof(dest->socket));

		if (res == SOCKET_ERROR)
		{
			res = errno;
			Print(DBG_ERROR("Failed to send message! (" + ToString(res) + ")"));
			return res;
		}

		return 0;
	}

	int8_t Socket::Receive(tstring* message, Connection* pConnection)
	{
		size_t recv_len = 0;
		char* buffer = (char*)malloc(m_bufferSize);
		memset(buffer, '\0', m_bufferSize);

		sockaddr_in client;
		socklen_t address_length = sizeof(client);

		if (m_protocol == Protocol::UDP)
			recv_len = recvfrom(m_socket, buffer, m_bufferSize, 0, (struct sockaddr *)&client, &address_length);
		else if (m_protocol == Protocol::TCP)
			recv_len = recv(m_connectedSocket, buffer, m_bufferSize, 0);
		if (recv_len != SOCKET_ERROR)
		{
			*message = tstring(buffer);
			if (m_protocol == Protocol::UDP)
			{
				pConnection->socket = client;
			}
			else if (m_protocol == Protocol::TCP)
			{

			}
		}
		else if (recv_len == 0)
		{
			Print(DBG_ERROR("Connection closed by client!"));
			closesocket(m_connectedSocket);
		}
		else
		{
			int8_t res = errno;
			Print(DBG_ERROR("Failed to receive message! (" + ToString(res) + ")"));
			return res;
		}

		return 0;
	}
}

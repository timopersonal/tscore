#include "../include/vfs.h"

#include "../include/console.h"
#include "../include/debug.h"

namespace tscore
{
	VFS::VFS(const String& root)
	{
		m_vfsMap = std::unordered_map<String, String>();
		m_vfsMap.insert(std::pair<String, String>("", root));
	}

	int VFS::Mount(const String& physicalPath, const String& virtualPath)
	{
		if (virtualPath.find('/') != String::npos)
		{
			Print(DBG_ERROR("VFS failed to mount: Virtual path contains illegal character: \'/\'"));
			return -1;
		}

	//	m_vfsMap.insert(std::pair<tstring, tstring>(virtualPath, physicalPath));
		m_vfsMap[virtualPath] = physicalPath;

		Print(DBG_DEBUG("VFS successfully mounted \'" + physicalPath + "\' to virtual \'" + virtualPath + "\'"));
		return 0;
	}

	ReadFile* VFS::OpenFile(const String& fileName)
	{
		Print(DBG_DEBUG("VFS retrieving file \'" + fileName + "\'"));
		size_t directoryDivIndex = fileName.find('/');
		if (directoryDivIndex != String::npos)
		{
			String dirName  = fileName.substr(0, directoryDivIndex);
			String restName = fileName.substr(directoryDivIndex + 1, fileName.length() - (directoryDivIndex + 1));
			if (m_vfsMap.find(dirName) != m_vfsMap.end())
			{
				String realDir = m_vfsMap.at(dirName);
				return new ReadFile(realDir + "/" + restName);
			}
			else
			{
				Print(DBG_ERROR("VFS virtual folder \"" + dirName + "\" does not exist!"));
				return nullptr;
			}
		}
		else
		{
			return new ReadFile(m_vfsMap.at("") + "/" + fileName);
		}

		return nullptr;
	}
}

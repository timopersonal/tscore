#include "../include/string.h"

#include <string.h>
#include <stdio.h>

namespace tscore
{
    String::String() :
        String("")
    {
    }

    String::String(const char* inChars)
    {
        m_length = strlen(inChars);
        m_capacity = m_length + 1;
        m_charArray = new char[m_capacity];
        m_charArray[m_length] = '\0';
        if (m_length > 0)
            strcpy(m_charArray, inChars);
    }

    String::~String()
    {
        m_charArray = 0;
        m_capacity = 0;
        m_length = 0;
    }

    size_t String::find(char c, size_t pos) const
    {
        size_t retVal = String::npos;

        for (int i = pos; i < m_length; ++i)
        {
            if (m_charArray[i] == c)
            {
                retVal = i;
                break;
            }
        }

        return retVal;
    }

    String String::substr(size_t offset, size_t n) const
    {
        char* temp = (char*)malloc(sizeof(char) * (n + 1));
        temp[n] = '\0';
        memcpy((void*)temp, m_charArray + offset, n);

        String retVal = String(temp);
        free(temp);
        return retVal;
    }

    String String::operator=(const String& rhs)
    {
        if (this != &rhs)
        {
            delete[] this->m_charArray;
            this->m_charArray = NULL;

            this->m_length = rhs.length();
            this->m_capacity = m_length + 1;

//            delete[] this->m_charArray;
            this->m_charArray = new char[m_capacity * sizeof(char)];
            this->m_charArray[m_length] = '\0';
//            memcpy(this->m_charArray, rhs.m_charArray, str_length);
            memcpy(this->m_charArray, rhs.c_str(), m_length);
        }

        return *this;
    }

    String String::operator+(const String& other) const
    {
        String result = *this;
        result += other;
        return result;
    }

    void String::operator+=(const String& rhs)
    {
        int old_size = m_length;
        resize(m_length + rhs.length());
        memcpy(m_charArray + old_size, rhs.c_str(), rhs.length());
    }

    bool String::operator==(const String& rhs) const
    {
        if (m_length != rhs.length())
            return false;

        for (int i = 0; i < m_length; ++i)
        {
            if (m_charArray[i] != rhs[i])
                return false;
        }

        return true;
    }

    void String::increase_capacity(size_t cap)
    {
        if (cap <= m_capacity) return;
        m_capacity = cap;

        char* temp = m_charArray;
        m_charArray = nullptr;
        m_charArray = new char[m_capacity + 1];
        m_charArray[m_capacity - 1] = '\0';
        if (temp)
            memcpy(m_charArray, temp, m_length);

        delete[] temp;
    }

    void String::resize(size_t size)
    {
        m_length = size;
        if (size >= m_capacity)
        {
            int cap = m_capacity * 2;
            cap = cap > size ? cap : size;
            increase_capacity(cap);
        }

        m_charArray[m_length] = 0x0;
    }

    String operator+(const char* lhs, const String& rhs)
    {
        return String(lhs) + rhs;
    }
}
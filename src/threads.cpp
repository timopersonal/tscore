#include "../include/threads.h"

#include <chrono>

namespace tscore
{
	void Thread::Sleep(uint32_t ms)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(ms));
	}

	void Thread::Await()
	{
		m_thread.join();
	}
}

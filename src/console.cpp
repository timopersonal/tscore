#include "../include/console.h"

#include <stdio.h>

namespace tscore
{
	void Print(const tstring& x)
	{
		if (x.length() > 0)
			printf("%s\n", x.c_str());
	}

	void Print(const String& x)
	{
		if (x.length() > 0)
			printf("%s\n", x.c_str());
	}
}

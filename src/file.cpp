#include "../include/file.h"

#include <string.h>

#include "../include/console.h"
#include "../include/debug.h"

namespace tscore
{
	IFile::IFile() :
		m_pFile(0),
		m_fileLength(0)
	{

	}

	IFile::~IFile()
	{
		if (m_pFile) fclose(m_pFile);
	}

	int IFile::MoveCursor(long int offset, int mode)
	{
		int begin = 0;
		if (mode == SEEK_END)
			begin = fseek(m_pFile, 0 , SEEK_END);
		if (mode == SEEK_CUR)
			begin = ftell(m_pFile);

		int res = fseek(m_pFile, offset, mode);

		if (res) return -1;
		else if (mode == SEEK_END)
			return begin - offset;
		else
			return begin + offset;
	}

	WriteFile::WriteFile(const String& fileName, bool binary, WriteMode writeMode) :
		IFile()
	{
		if (writeMode == WRITE_CREATEONLY)
		{
			FILE* pTemp = fopen(fileName.c_str(), "rb");
			if (pTemp)
			{
				Print(DBG_ERROR("File \"" + fileName + "\" already exists!"));
				fclose(pTemp);
				return;
			}
		}

		bool append = writeMode == WRITE_APPEND;
		m_pFile = fopen(fileName.c_str(), (String((append) ? "a" : "w") + String((binary) ? "b" : "")).c_str());
		m_fileName = fileName;
		if (!m_pFile)
		{
			Print(DBG_ERROR("Failed to open file \'" + fileName + "\'"));
			return;
		}

		Print(DBG_DEBUG("Successfully opened file \'" + fileName + "\' for writing."));
	}

	bool WriteFile::Write(const String& stream)
	{
		size_t streamlen = stream.length();
		size_t writelen  = fwrite(stream.c_str(), 1, streamlen, m_pFile);
		return (streamlen == writelen);
	}

	WriteFile::~WriteFile()
	{

	}

	ReadFile::ReadFile(const String& fileName, bool binary) :
		IFile()
	{
		m_fileName = fileName;
		m_pFile = fopen(fileName.c_str(), (String("r") + String((binary) ? "b" : "")).c_str());
		if (!m_pFile)
		{
			Print(DBG_ERROR("Failed to open file " + fileName));
			return;
		}

		fseek(m_pFile, 0, SEEK_END);
		m_fileLength = ftell(m_pFile);
		fseek(m_pFile, 0, SEEK_SET);

		Print(DBG_DEBUG("Successfully opened file \'" + fileName + "\' for reading."));
	}

	String ReadFile::Read(unsigned int num)
	{
		char* buffer = (char*)malloc(num);
		fread(buffer, 1, num, m_pFile); // TODO: Error handling
		String ret(buffer);
		free(buffer);

		return ret;
	}

	bool ReadFile::IterateLine(bool cursorEndLine)
	{
		int begin = ftell(m_pFile);

		/* note that fgets doesn't strip the terminating \n, checking its
		   presence would allow to handle lines longer that sizeof(line) */
	    bool retVal = (bool)fgets(currentLineBuffer, READ_BUFFER_LENGTH, m_pFile);

		uint32_t lineLength = strlen(currentLineBuffer);
		int newLineCompensation = (int)(currentLineBuffer[lineLength - 1] == '\n');
		currentLine = String(tstring(currentLineBuffer, 0, lineLength - newLineCompensation).c_str());

		if (!cursorEndLine) fseek(m_pFile, begin, SEEK_SET);

		return retVal;
	}

	ReadFile::~ReadFile()
	{

	}
}

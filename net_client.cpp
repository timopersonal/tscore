#include "include/console.h"
#define DBG_LEVEL DBG_LEVEL_ALL
#include "include/debug.h"
#include "include/file.h"
#include "include/platform.h"
#include "include/sockets.h"
#include "include/timing.h"
#include "include/types.h"

#include <string.h>

using namespace tscore;

int main(int argc, char** argv)
{
	Socket client = Socket(Protocol::UDP, 8080);
	client.Connect("127.0.0.1");

	char command[50];
	while (tstring(command) != "/exit\n")
	{
		memset(command, 0, sizeof(command));
		fgets(command, sizeof(command), stdin);
		client.Send(client.GetConnection(), tstring(command));

		tstring message;
		Connection sender;
		client.Receive(&message, &sender);
		Print(message);
	}

	return 0;
}
